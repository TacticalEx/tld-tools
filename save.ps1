param (
	[Parameter()]
    [string]$operation,
    [string]$sourcePath,
    [string]$backupPath,
    [string]$alert
)

$validOperations = "backup", "restore"
$global:saveSource = "C:\Users\[yourname]\AppData\Local\Hinterland" # Provide the full path to the save location here
$global:backupLocation = "C:\Users\[yourname]\TheLongDarkBackup\continuousBackup" # Provide the full path the desired backup location here

# Backs up a game data directory to another location
function backup
{
    Get-ChildItem -Path $saveSource | Copy-Item -Destination $backupLocation -Recurse -Container -Force
    if ($alert.ToLower() -ne "none") {
        $wshell = New-Object -ComObject Wscript.Shell
        $wshell.Popup("Backup Completed",3,"TLD Tools",0x0 + 0x40 + 4096)
    }
}

# Restores the game data from a previous backup to the game data directory
function restore
{
    function executeRestore {
        Get-ChildItem -Path $backupLocation | Copy-Item -Destination $saveSource -Recurse -Container -Force
    }
    
    if ($alert.ToLower() -ne "none") {
        $wshell = New-Object -ComObject Wscript.Shell
        $answer = $wshell.Popup("Restore initiated. This is a potentially damaging operation, are you sure?",0,"TLD Tools",0x4 + 0x30 + 4096)
        if ($answer -eq 6){
            executeRestore
        }
    } else {
        executeRestore
    }
}

# Validate paths
function validatePaths {
    # If a source path was passed in, overwrite the default path with it
    if ([string]::IsNullOrEmpty($sourcePath) -eq $False){
        $global:saveSource = $sourcePath
    }
    # If a backup path was passed in, overwrite the default path with it
    if ([string]::IsNullOrEmpty($backupPath) -eq $False){
        $global:backupLocation = $backupPath
    }
    $pathsToCheck = $saveSource, $backupLocation
    ForEach ($path in $pathsToCheck) {
        if ((Test-Path -Path $path) -eq $False) {
            Throw "Invalid path of $path provided"
        }
    }
}

$operation = $operation.ToLower() # force to lower case to avoid comparison issues
# Validate input and execute provided operation
if ($validOperations.contains($operation) -eq $False) {
    # Do nothing and return error message
    Throw "operation '$operation' not recognized"
} else {
    validatePaths
    switch ($operation) {
        "backup" { backup }
        "restore" { restore }
        Default { backup } # Theoretically never encountered @ignoreCodeCoverage
    }
}
