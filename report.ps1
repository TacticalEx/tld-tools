param (
    [string]$operationPath,
    [string]$outputName
)

$global:saveSource = "S:\gitprojects\tld-tools\sandbox\backup" # Provide the full path to the location to report on

function report 
{
    # sets a file name if not passed in
    if ([string]::IsNullOrEmpty($outputName)){
        $outputName = "default"
    }

    # Sets the path the report will run for to the save location if no override is provided
    if ([string]::IsNullOrEmpty($operationPath) -eq $False){
        $saveSource = $operationPath
    }

    $out = Get-ChildItem $saveSource -File -Recurse 
    Out-File -InputObject $out "$outputName.txt"
}

report