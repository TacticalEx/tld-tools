# Init PowerShell Gui
Add-Type -AssemblyName System.Windows.Forms
Add-Type -AssemblyName System.Drawing

# Set Value for autosave timer
$script:running = $false

# Timer to execute the autosave method
$timer = New-Object System.Windows.Forms.Timer
$timer.Interval = 60000 * 10

# Create a new form
$mainForm = New-Object system.Windows.Forms.Form

# Define the theme and UI elements
# icon
$mainForm.Icon = [System.Drawing.Icon]::ExtractAssociatedIcon("$PSScriptRoot\assets\simple_tools_tiny_icon.ico")

# Color scheme 
$colorBackDrop = "#0D100F" # Dark gray
$colorAccent = "#F9F9F9" # gray (text)
$colorBright = "#373A3A" # Faded white (active select) 
$colorStop = "#CE9F45" # yellow (condition low)
$colorStart = "#6E837C" # green (good condition)
$colorTextBoxCharacters = "#F9F9F9"
$colorButtonText  = "#F9F9F9"
$mainForm.ClientSize        = '500,300'
$mainForm.text              = "TLD Tools v2.1"
$mainForm.BackColor         = $colorBackDrop

# Set Font
# Specify the path to font file
$fontFilePath = "$PSScriptRoot\assets\Oswald-VariableFont_wght.ttf"

# Create a PrivateFontCollection
$fontCollection = New-Object System.Drawing.Text.PrivateFontCollection

# Check if the font file exists
if (Test-Path $fontFilePath) {
    # Load the font from the file
    $fontCollection.AddFontFile($fontFilePath)

    # Use the first font in the collection
    $font = $fontCollection.Families[0]
}
else {
    # Output a message if the font file is not found
    Write-Host "Font not found. Using default font."

    # Use a default font
    $font = New-Object Drawing.FontFamily("Microsoft Sans Serif") # Microsoft Sans Serif
}

################
# Create Labels
################
# Status label
$statusLbl                  = New-Object system.Windows.Forms.Label
$statusLbl.text             = "Status:"
$statusLbl.AutoSize         = $true
$statusLbl.location         = New-Object System.Drawing.Point(10,280)
$statusLbl.Font             = New-Object Drawing.Font($font,10,[System.Drawing.FontStyle]::Bold)
$statusLbl.ForeColor        = $colorAccent

# Source label
$sourceLbl                  = New-Object system.Windows.Forms.Label
$sourceLbl.text             = "Original save location"
$sourceLbl.AutoSize         = $true
$sourceLbl.location         = New-Object System.Drawing.Point(10,10)
$sourceLbl.Font             = New-Object Drawing.Font($font, 12)
$sourceLbl.ForeColor        = $colorAccent

# Backup label
$backupLbl                  = New-Object system.Windows.Forms.Label
$backupLbl.text             = "Backup location"
$backupLbl.AutoSize         = $true
$backupLbl.location         = New-Object System.Drawing.Point(10,40)
$backupLbl.Font             = New-Object Drawing.Font($font,12)
$backupLbl.ForeColor        = $colorAccent

# hotkey label
$hotkeyLbl                  = New-Object system.Windows.Forms.Label
$hotkeyLbl.text             = "Hotkey"
$hotkeyLbl.AutoSize         = $true
$hotkeyLbl.location         = New-Object System.Drawing.Point(10,76)
$hotkeyLbl.Font             = New-Object Drawing.Font($font,12,[System.Drawing.FontStyle]::Bold)
$hotkeyLbl.ForeColor        = $colorAccent

# modifier label
$modifierLbl                  = New-Object system.Windows.Forms.Label
$modifierLbl.text             = "Modifier:"
$modifierLbl.AutoSize         = $true
$modifierLbl.location         = New-Object System.Drawing.Point(258,76)
$modifierLbl.Font             = New-Object Drawing.Font($font,12,[System.Drawing.FontStyle]::Bold)
$modifierLbl.ForeColor        = $colorAccent

# autosave label
$autosaveLbl                  = New-Object system.Windows.Forms.Label
$autosaveLbl.text             = "Autosave interval"
$autosaveLbl.AutoSize         = $true
$autosaveLbl.location         = New-Object System.Drawing.Point(10,112)
$autosaveLbl.Font             = New-Object Drawing.Font($font,12,[System.Drawing.FontStyle]::Bold)
$autosaveLbl.ForeColor        = $colorAccent

# autosave Min label
$autosaveMinLbl                  = New-Object system.Windows.Forms.Label
$autosaveMinLbl.text             = "(min)"
$autosaveMinLbl.AutoSize         = $true
$autosaveMinLbl.location         = New-Object System.Drawing.Point(196,112)
$autosaveMinLbl.Font             = New-Object Drawing.Font($font,10,[System.Drawing.FontStyle]::Bold)
$autosaveMinLbl.ForeColor        = $colorAccent

# Backup Set label
$backupsetLbl                  = New-Object system.Windows.Forms.Label
$backupsetLbl.text             = "Backup Set limit"
$backupsetLbl.AutoSize         = $true
$backupsetLbl.location         = New-Object System.Drawing.Point(10,152)
$backupsetLbl.Font             = New-Object Drawing.Font($font,12,[System.Drawing.FontStyle]::Bold)
$backupsetLbl.ForeColor        = $colorAccent

# Backup list label
$backupListLbl                  = New-Object system.Windows.Forms.Label
$backupListLbl.text             = "Backups:"
$backupListLbl.AutoSize         = $true
$backupListLbl.location         = New-Object System.Drawing.Point(190,152)
$backupListLbl.Font             = New-Object Drawing.Font($font,12,[System.Drawing.FontStyle]::Bold)
$backupListLbl.ForeColor        = $colorAccent

####################
# Create Text boxes
####################
# source path text box
$sourceTxtBx                = New-Object system.Windows.Forms.TextBox
$sourceTxtBx.multiline      = $false
$sourceTxtBx.width          = 328
$sourceTxtBx.height         = 20
$sourceTxtBx.location       = New-Object System.Drawing.Point(160,8)
$sourceTxtBx.Font           = New-Object Drawing.Font($font,10)
$sourceTxtBx.ForeColor      = $colorTextBoxCharacters
$sourceTxtBx.BackColor      = $colorBright
$sourceTxtBx.Visible        = $true
$sourceTxTBx.TabIndex       = 6

# backup path text box
$backupTxtBx                = New-Object system.Windows.Forms.TextBox
$backupTxtBx.multiline      = $false
$backupTxtBx.width          = 328
$backupTxtBx.height         = 20
$backupTxtBx.location       = New-Object System.Drawing.Point(160,36)
$backupTxtBx.Font           = New-Object Drawing.Font($font,10)
$backupTxtBx.ForeColor      = $colorTextBoxCharacters
$backupTxtBx.BackColor      = $colorBright
$backupTxtBx.Visible        = $true
$backupTxtBx.TabIndex       = 7

# hotkey text box
$hotkeyTxtBx                = New-Object system.Windows.Forms.TextBox
$hotkeyTxtBx.multiline      = $false
$hotkeyTxtBx.width          = 26
$hotkeyTxtBx.height         = 20
$hotkeyTxtBx.location       = New-Object System.Drawing.Point(160,70)
$hotkeyTxtBx.Font           = New-Object Drawing.Font($font,12)
$hotkeyTxtBx.BackColor      = $colorBright
$hotkeyTxtBx.Visible        = $true
$hotkeyTxtBx.MaxLength      = 1

# autosave text box
$autosaveIntTxtBx           = New-Object system.Windows.Forms.TextBox
$autosaveIntTxtBx.multiline = $false
$autosaveIntTxtBx.width     = 36
$autosaveIntTxtBx.height    = 20
$autosaveIntTxtBx.location  = New-Object System.Drawing.Point(160,106)
$autosaveIntTxtBx.Font      = New-Object Drawing.Font($font,12)
$autosaveIntTxtBx.ForeColor = $colorTextBoxCharacters
$autosaveIntTxtBx.BackColor = $colorBright
$autosaveIntTxtBx.Visible   = $true
$autosaveIntTxtBx.MaxLength = 3
$autosaveIntTxtBx.TabIndex  = 8

# backup set text box
$backupSetTxtBx           = New-Object system.Windows.Forms.TextBox
$backupSetTxtBx.multiline = $false
$backupSetTxtBx.width     = 26
$backupSetTxtBx.height    = 20
$backupSetTxtBx.location  = New-Object System.Drawing.Point(160,146)
$backupSetTxtBx.Font      = New-Object Drawing.Font($font,12)
$backupSetTxtBx.ForeColor = $colorTextBoxCharacters
$backupSetTxtBx.BackColor = $colorBright
$backupSetTxtBx.Visible   = $true
$backupSetTxtBx.MaxLength = 2
$backupSetTxtBx.TabIndex  = 9

####################
# Create Combo Boxes
####################
# Modifier Key
$modKeyComboBox                     = New-Object system.Windows.Forms.ComboBox
$modKeyComboBox.text                = ""
$modKeyComboBox.BackColor           = $colorBright
$modKeyComboBox.width               = 70
$modKeyComboBox.height              = 20
@('ALT','CTRL','SHIFT') | ForEach-Object {[void] $modKeyComboBox.Items.Add($_)}
$modKeyComboBox.SelectedIndex       = 0
$modKeyComboBox.location            = New-Object System.Drawing.Point(330,72)
$modKeyComboBox.Font                = New-Object Drawing.Font($font,10)
$modKeyComboBox.Visible             = $true

# backup list
$backupListComboBox                     = New-Object system.Windows.Forms.ComboBox
$backupListComboBox.text                = "Select backup to restore"
@('No backups detected') | ForEach-Object {[void] $backupListComboBox.Items.Add($_)}
$backupListComboBox.ForeColor           = $colorTextBoxCharacters
$backupListComboBox.BackColor           = $colorBright
$backupListComboBox.width               = 230
$backupListComboBox.height              = 20
$backupListComboBox.SelectedIndex       = 0
$backupListComboBox.location            = New-Object System.Drawing.Point(260,146)
$backupListComboBox.Font                = New-Object Drawing.Font($font,10)
$backupListComboBox.Visible             = $true
$backupListComboBox.TabIndex            = 3

################
# Create buttons
################
# autoSave button
$autoSaveButton               = New-Object Windows.Forms.Button
$autoSaveButton.Text          = "Autosave - start"
$autoSaveButton.ForeColor     = $colorButtonText
$autoSaveButton.BackColor     = $colorStart
$autoSaveButton.Font          = New-Object Drawing.Font($font,12, [System.Drawing.FontStyle]::Bold)
$autoSaveButton.Size          = New-Object Drawing.Size(140, 30)
$autoSaveButton.Location      = New-Object Drawing.Point(260, 106)
$autoSaveButton.TabIndex      = 2  

# Backup button
$backupButton               = New-Object Windows.Forms.Button
$backupButton.Text          = "Backup"
$backupButton.ForeColor     = $colorButtonText
$backupButton.BackColor     = $colorBright
$backupButton.Font          = New-Object Drawing.Font($font,14)
$backupButton.Size          = New-Object Drawing.Size(100, 60)
$backupButton.Location      = New-Object Drawing.Point(40, 200)
$backupButton.TabIndex      = 1  

# Save configuration button
$saveConfigButton           = New-Object Windows.Forms.Button
$saveConfigButton.Text      = "Save config"
$saveConfigButton.ForeColor = $colorButtonText
$saveConfigButton.BackColor = $colorBright
$saveConfigButton.Font      = New-Object Drawing.Font($font,14)
$saveConfigButton.Size      = New-Object Drawing.Size(100, 60)
$saveConfigButton.Location  = New-Object Drawing.Point(190, 200)
$saveConfigButton.TabIndex  = 10

# restore button
$restoreButton              = New-Object Windows.Forms.Button
$restoreButton.Text         = "Restore"
$restoreButton.ForeColor    = $colorButtonText
$restoreButton.BackColor    = $colorBright
$restoreButton.Font         = New-Object Drawing.Font($font,14)
$restoreButton.Size         = New-Object Drawing.Size(100, 60)
$restoreButton.Location     = New-Object Drawing.Point(340, 200)
$restoreButton.TabIndex     = 4

################################
# Define the button click events
################################
$autoSaveButton.Add_Click({
    if ($script:running) {
        Stop-Execution
    } else {
        Start-Execution
    }
})

$backupButton.Add_Click({
    runBackup
})

$saveConfigButton.Add_Click({
    SaveToJsonFile -sourcePath $sourceTxtBx.Text -backupPath $backupTxtBx.Text -autoSaveInterval $autosaveIntTxtBx.Text -backupSetLimit $backupSetTxtBx.Text
})

$restoreButton.Add_Click({
    runRestore
})

#####################
# Functions
#####################
# execute a single backup with current config
function runBackup {
    # Store text values
    $sourceLocation = $sourceTxtBx.Text.Trim()
    $backupLocation = $backupTxtBx.Text.Trim()

    # Add unique folder name to the backup location
    $timestamp = Get-Date -Format "yyyy-MM-dd-hh-mm-ss"
    $backupLocation = "$backupLocation\backup-$timestamp"

    # create the unique folder
    New-Item -ItemType Directory -Path $backupLocation -Force | Out-Null

    # Execute the save script (backup action)
    & "$PSScriptRoot\save.ps1" -operation backup -sourcePath "$sourceLocation" -backupPath "$backupLocation" -alert none

    # provide status update
    updateStatusLabel -message "created backup-$timestamp"

    # clean up backup set
    cleanUpBackups
}

# restored the currently selected backup to the source
function  runRestore {
    $sourceLocation = $sourceTxtBx.Text.Trim()
    $backupLocation = $backupTxtBx.Text.Trim()
    
    # Get the selected value from the combo box
    $selectedValue = $backupListComboBox.SelectedItem

    # Create the final file path by combining the base path and the selected value
    $backupLocation = Join-Path $backupLocation $selectedValue
    
    # Execute the save script (restore action)
    & "$PSScriptRoot\save.ps1" -operation restore -sourcePath "$sourceLocation" -backupPath "$backupLocation" -alert none

    updateStatusLabel -message "$selectedValue restored"
}

function cleanUpBackups{
    # Set the target directory
    $targetDirectory = $backupTxtBx.Text.Trim()

    # Get all subfolders that match the "backup-" pattern
    $backupFolders = Get-ChildItem -Path $targetDirectory -Directory | Where-Object { $_.Name -match "^backup-" }

    # Set the desired number of folders to retain
    $desiredCount = [int]::Parse($backupSetTxtBx.Text)

    # If the number of backup folders is already less than or equal to the desired count, exit
    if ($backupFolders.Count -le $desiredCount) {
        # Do nothing
    } else {
        # Sort the folders by creation time in ascending order
        $oldestFolders = $backupFolders | Sort-Object CreationTime

        # Calculate the number of folders to delete
        $foldersToDelete = $backupFolders.Count - $desiredCount

        # Delete the oldest folders
        $deletedCount = 0
        do {
            $folderToDelete = $oldestFolders[$deletedCount]
            Remove-Item -Path $folderToDelete.FullName -Recurse -Force
            $deletedCount++
        } while ($deletedCount -lt $foldersToDelete)
    }

    # Repopulate the backup list for the combo box
    populateRestoreList
}

function SaveToJsonFile {
    param (
        [string]$sourcePath,
        [string]$backupPath,
        [string]$hotkey,
        [string]$hotkeyMod,
        [string]$backupSetLimit,
        [string]$autoSaveInterval,
        [string]$filePath = "$PSScriptRoot\data\data.json"
    )

    $jsonObject = @{
        "sourcePath" = $sourcePath
        "backupPath" = $backupPath
        "hotkey" = $hotkey
        "hotkeyModifer" = $hotkeyMod
        "backupSetLimit" = $backupSetLimit
        "autoSaveInterval" = $autoSaveInterval
    }

    $jsonContent = $jsonObject | ConvertTo-Json -Depth 1
    $jsonContent | Out-File -FilePath $filePath -Encoding UTF8

    updateStatusLabel -message "Saved configuration"
}

# Replace the $jsonString variable with your JSON content
function LoadFromJsonFile {
    param (
        [string]$filePath = "$PSScriptRoot\data\data.json"
    )

    if (Test-Path $filePath) {
        $jsonContent = Get-Content -Path $filePath -Raw
        $jsonObject = $jsonContent | ConvertFrom-Json
        updateStatusLabel -message "Configuration data loaded from JSON"
        return $jsonObject
    } else {
        updateStatusLabel -message "Failed to load JSON data"
        return $null
    }
}

# Changes the status label message and appends timestamp
function updateStatusLabel {
    param (
        [string]$message
    )

    $timestamp = Get-Date -Format "hh:mm:ss tt on MM/dd"
    $statusLbl.text = "Status: $message at $timestamp"
}

# Creates the list of backups for the combo box to select
function populateRestoreList {
    # path to search in
    $backupPath = $backupTxtBx.Text.Trim()

    # Search for folders that start with "backup-" in the specified directory
    $backupFolders = Get-ChildItem -Path $backupPath -Directory | Where-Object { $_.Name -like 'backup-*' }

    # Clear the ComboBox items before populating it
    $backupListComboBox.Items.Clear()

    # Populate the ComboBox with the names of the found backup folders
    foreach ($folder in $backupFolders) {
        $backupListComboBox.Items.Add($folder.Name)
    }

    if ($backupFolders.Count -lt 1) {
        # If no backup folders are found, set the ComboBox text to the message below
        $backupListComboBox.Text = "No backup folders found."
    } else {
        # Populate the ComboBox with the names of the found backup folders
        $backupListComboBox.Text = "Select a backup to restore"
    }
}

# Makes sure that the autosave Interval value is a valid number
function validateAutosaveInterval {
    param (
        [string]$interval
    )

    $intValue = 0
    [int]::TryParse($interval, [ref]$intValue) | Out-Null
    return $intValue
}

# Stop Autosave
function Stop-Execution {
    $script:running = $false
    $timer.Stop()
    $autoSaveButton.ForeColor = $colorButtonText
    $autoSaveButton.BackColor = $colorStart
    $autoSaveButton.Text = "Autosave - start"
    updateStatusLabel -message "autosave stopped"
}

# start autosave
function Start-Execution {
    $inputValue = $autosaveIntTxtBx.Text
    $intValue = validateAutosaveInterval $inputValue
    
    if ($intValue -gt 0){
        $script:running = $true
        $timer.Interval = 60000 * $intValue # 60000 is minute
        $timer.Start()
        $autoSaveButton.ForeColor = $colorBright
        $autoSaveButton.BackColor = $colorStop
        $autoSaveButton.Text = "Autosave - stop"
        updateStatusLabel -message "autosave started"
    } else {
        updateStatusLabel -message "unable to start autosave"
    }
}

#################
# initialization
#################

# Load the data from the JSON file
$jsonObject = LoadFromJsonFile

# Set the values from json data to the textboxes
$sourceTxtBx.Text = $jsonObject.sourcePath
$backupTxtBx.Text = $jsonObject.backupPath
# $hotkeyTxtBx.Text = $jsonObject.hotkey
$autosaveIntTxtBx.Text = $jsonObject.autoSaveInterval
$backupSetTxtBx.Text = $jsonObject.backupSetLimit

# Initialize the list of possible restore points
populateRestoreList

# Set the focus on the button when the form is loaded
$mainForm.Add_Shown({ $backupButton.Focus() })

# Registering the timer event with the form
$mainForm.Add_Load({ $timer.Start() })

# method to run when the timer is active
$timer.Add_Tick({
    if ($script:running) {
        runBackup
    }
})

# Add elements to the form
$mainForm.controls.AddRange(@(
    $sourceTxtBx, 
    $saveConfigButton, 
    $backupTxtBx,
    $backupButton,
    $restoreButton,
    $statusLbl,
    $sourceLbl,
    $backupLbl,
    # $hotkeyLbl,
    # $hotkeyTxtBx,
    # $modifierLbl,
    # $modKeyComboBox,
    $autosaveLbl,
    $autosaveMinLbl,
    $autosaveIntTxtBx,
    $autoSaveButton,
    $backupsetLbl,
    $backupListLbl,
    $backupSetTxtBx,
    $backupListComboBox
))

# Display the form
[void]$mainForm.ShowDialog()

# Stop the timer when the form is closed
$timer.Stop()