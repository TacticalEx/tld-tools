param (
    [Parameter()]    
    [string]$operation,
    [string]$sourcePath,
    [string]$backupPath,
    [string]$alert
)

Describe "GIVEN no options are used" {
    Context "WHEN no arguments are passed in" {
        It "THEN an exception is thrown"{
            { .\save.ps1 } |
                Should -Throw "operation '' not recognized"
        }
    }
    Context "WHEN no value for [operation] is passed in" {
        It "THEN an exception is thrown"{
            { .\save.ps1 -operation} |
                Should -Throw "Missing an argument for parameter 'operation'. Specify a parameter of type 'System.String' and try again."
        }
    }
    Context "WHEN no value for [sourcePath] is passed in" {
        It "THEN an exception is thrown"{
            {.\save.ps1 -sourcePath} |
                Should -Throw "Missing an argument for parameter 'sourcePath'. Specify a parameter of type 'System.String' and try again."
        }
    }
    Context "WHEN no value for [backupPath] is passed in" {
        It "THEN an exception is thrown"{
            {.\save.ps1 -backupPath} |
                Should -Throw "Missing an argument for parameter 'backupPath'. Specify a parameter of type 'System.String' and try again."
        }
    }
    Context "WHEN no value for [alert] is passed in" {
        It "THEN an exception is thrown"{
            {.\save.ps1 -alert} |
                Should -Throw "Missing an argument for parameter 'alert'. Specify a parameter of type 'System.String' and try again."
        }
    }
}

Describe "GIVEN [operation] value is passed in" {
    BeforeEach {
        # Create test SOURCE directory and populate with unique files
        $testPath = "TestDrive:\tempDir"
        New-Item -Path "$testPath" -Name "Source\Alpha.txt" -ItemType "file" -Force
        Set-Content "$testPath\Source\Alpha.txt" -value "SourceValue"
        New-Item -Path "$testPath\Source\sub\" -Name "Beta.txt" -ItemType "file" -Force

		# Create test backup directory and populate with unique files
		New-Item -Path "$testPath" -Name "Backup" -ItemType "directory"
		Set-Content -Path "$testPath\Backup\Alpha.txt" -Value 'BackupValue'
		New-Item -Path "$testPath\Backup\sub\" -Name "Gamma.txt" -ItemType "file" -Force
    }
    AfterEach {
		Remove-Item "$testPath" -Recurse
	}
    Context "WHEN the operation is a valid upper case string [BACKUP]" {
        It "THEN files of the same name are over-written" {
            .\save.ps1 -operation "BACKUP" -sourcePath "$testPath\Source" -backupPath "$testPath\Backup" -alert none
            "$testPath\Backup\Alpha.txt" | Should -FileContentMatch "SourceValue"
        }
        It "THEN new files are added to the directory" {
            .\save.ps1 -operation "BACKUP" -sourcePath "$testPath\Source" -backupPath "$testPath\Backup" -alert none
            "$testPath\Backup\sub\Beta.txt" | Should -Exist
        }
    }

    Context "WHEN the operation is valid mixed case string [resTOre]" {
        It "THEN files of the same name are over-written" {
            .\save.ps1 -operation "resTOre" -sourcePath "$testPath\Source" -backupPath "$testPath\Backup" -alert none
            "$testPath\Source\Alpha.txt" | Should -FileContentMatch "BackupValue"
        }
        It "THEN new files are added to the directory" {
            .\save.ps1 -operation "resTOre" -sourcePath "$testPath\Source" -backupPath "$testPath\Backup" -alert none
            "$testPath\Source\sub\Gamma.txt" | Should -Exist
        }
    }

    Context "WHEN the operation is an invalid lower case string [wolf]" {
        It "THEN and error occurs" {
            {.\save.ps1 -operation "wolf"} |
                Should -Throw "operation 'wolf' not recognized"
        }
    }
}

Describe "GIVEN invalid paths are provided" {
    BeforeEach {
        # Create test SOURCE directory and populate with unique files
		$testPath = "TestDrive:\tempDir"
        New-Item -Path "$testPath" -Name "Source\Alpha.txt" -ItemType "file" -Force
        Set-Content "$testPath\Source\Alpha.txt" -value "SourceValue"
        New-Item -Path "$testPath\Source\sub\" -Name "Beta.txt" -ItemType "file" -Force

		# # Create test backup directory and populate with unique files
		New-Item -Path "$testPath" -Name "Backup" -ItemType "directory"
		Set-Content -Path "$testPath\Backup\Alpha.txt" -Value 'BackupValue'
		New-Item -Path "$testPath\Backup\sub\" -Name "Gamma.txt" -ItemType "file" -Force
    }
    AfterEach {
        Remove-Item "$testPath" -Recurse
    }
    Context "WHEN the source path is not valid" {
        It "THEN an error is thrown" {
            {.\save.ps1 -operation backup -sourcePath "pineapple" -backupPath "$testPath\Backup" -alert none} |
                Should -Throw "Invalid path of pineapple provided"
        }
    }
    Context "WHEN the backup path is not valid" {
        It "THEN an error is thrown" {
            {.\save.ps1 -operation backup -sourcePath "$testPath\Source" -backupPath "pineapple" -alert none} |
            Should -Throw "Invalid path of pineapple provided"
        }
    }
}
