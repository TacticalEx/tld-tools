# TLD-Tools

![TLDTools GUI](readmeAssets/readme-screen.png)

Enjoy some simple tools for managing game saves for The Long Dark.

The Long Dark employs perma-death for immersion and risk-reward balance which is great for gameplay but not always forgiving when experimenting because your game will be deleted when your character drifts quietly into the long dark (you die). A game can be restored if it's data was copied to a location on the machine which the game does not manage. This is a relatively trivial process but having to do it is a minor annoyance so this utility assists in making it a less painful process. 

NOTE: This utility is not intended to remove the risk of perma-death. Ultimately, the choice of how to use this utility is yours but I highly encourage you to NOT USE THIS utility if you are searching solely for a way to save a survival mode game just to escape the cost of dying. I developed this to assist in testing some risky mechanics out and recover in the case of getting stuck and have found it useful to restore an old save when I encounter a game breaking bug. No garuntees are made for the use of this software. The user assumes all risk. 

## Requirements
 - OS: Windows 
 - Powershell version 5.1

##  UI mode

Version 2 introduced a GUI with some bells and whistles so that the utility is more accessible. 

### Features only availble to UI mode

* Backup sets: save up to 99 backups, automatically versioned and timestamped to restore from.
* Autosave: start and stop the autosave at a click of a button and set the autosave interval (0 to 999 minutes).
* Save configuration: Saves the current configuration (file paths, autosave interval, etc) to be used next time the application is started.

### Setup and Usage

1. Create a shortcut
    1. (Recommended) Double click on the file `Create TLDTools Shortcut.vbs`. This will create a shortcut with a visually pleasing icon in the same folder that you can cut/paste anywhere on you computer to execute the launcher

(OR)

1. Run the launcher batch file
    1. Double click on the file `TLDTools Lancher.bat`. This runs the powershell script that launches the application in hidden mode that will hide the powershell terminal. 

(OR)

1. Run the powershell script directly. 
    1. Right click on `TLDTools.ps1` and select `Run with Powershell`. This will execute the script with no parameters and open up the GUI and the powershell terminal window. Note that closing the terminal window will also close the GUI. 

Once the GUI has launched you will need to set up a `source` location and a `backup` location. 
1. Find where the game saves you want to backup are stored and copy that path into the `Original save location text field`. For most users, that location is `C:\Users\[username]\AppData\Local\Hinterland`. 
1. Designate a path to save backups. This can be anywhere you want, just be sure to provide the full path in the `Backup location` text field and make sure all of the sub directories exist before attempting to run a backup operation. 
1. Set the maximum number of backups you wish to keep by typing a number into the `backup set limit` text box. Any number from 0 to 99 is valid. Oldest backups are automatically deleted when new ones are created and exceed the limit set in the text box. 
1. Click on the backup button. 

If everything was set up correctly you will see the message `Select a backup to restore` in the backups drop down box. If you want to restore a save, simply select the save from the drop down and click on the restore button. 

#### Save config

Clicking on `Save config` will store all of the text box values and the application will load with all of the settings as they were set when the button was clicked the next time the application is opened. 

#### Autosave

Autosave can be turned on and off by clicking the `autosave` button. The status message at the bottom of the form and the color and text of the button will indicate it's current state. When activated, the backup process will be launched every `n` minutes. The number in the Autosave interval text box represents how many minutes to wait between creating backups and has a maximum value of 999.

#### Restore

Restore a backup by selecting a backup from the drop down list and then clicking on the "Restore" button. NOTE: It is possible to restore a backup while the game is currently running but this can cause some instability or outright fail to update the game's save information. It is highly recommended that you exit the game and perform a restore before starting the game again.   

## Command line mode

### Features only availble to command line mode

* report: Create informative reports about the state of the game data (currently this is limited and not accessible from the UI)

### Options

The current tools available support 3 actions: Backup, Restore and report 

### Backup and Restore

1. Setup: Before running the script, a backup and source location must be identified.
    - Your game data location may vary depending on your machine but a likely location is
        - C:\Users\[yourname]\AppData\Local\Hinterland
        - You will see two directories "TheLongDark" and "TheLongDarkNoSync" these are the directories which will be backed up
    1. Select which arguments to use 
    1. ```-operation``` REQUIRED - Decides which operation to perform
        - backup
        - restore
    1. ```sourcePath``` OPTIONAL - Points the script to the game saves source location
    1. ```backupPath``` OPTIONAL - Points the script to the game saves backup location
        - For convenience, the script can be updated to use paths stored as global variables in the script. In the event no paths are passed in with the operation, the script will attempt to use these fall-back paths.
            - ```$global:saveSource = "C:\Users\[yourname]\AppData\Local\Hinterland" # Provide the full path to the save location here```
            - ```$global:backupLocation = "C:\Users\[yourname]\TheLongDarkBackup\continuousBackup" # Provide the full path the desired backup location here```
    1. ```alert``` OPTIONAL - Sets the alert level of the operation. By default, the script will alert the user of a success (backup) or prompt for confirmation (restore). This can be turned off by providing "none" as an argument for the ```alert``` parameter.
        - Note: A pop up is triggered upon backup as a notification of success. This will automatically clear in 3 seconds if not acted upon by the user. The restore prompt will not automatically clear and requires user interaction if alerts are not surpressed.
1. Run the script 
    - Example: ```.\save.ps1 -operation backup -sourcePath "C:\Path\to\saves" -backupPath "C:\Path\to\Backup" -alert none```

### Report

This command currently only exports a text file with basic file information. Everything provided by this script can be gained by examining properties of the directory from the OS's file explorer. It's original purpose was to track last modified dates of files related to the game en-masse.  
1. Run the command ```.\report.ps1```
    - The ```operationPath``` parameter is the location of which the report will generate information from. 
        - Similarly to the save.ps1 script, there is a global variable to store the path for convenience, allowing the ```operationPath``` to be omitted once set.
        - ```$global:saveSource = "C:\Users\[yourname]\AppData\Local\Hinterland" # Provide the full path to the location to report on```
    - The ```outputName``` parameter stores the value of the file name provided by the user. If omitted the resultsing name will be "default"
1. Example: ```.\report.ps1 -operationPath "C:\Documents\info" -outputName myfile```
    - Currently, the output file appears in the same directory the script was executed from.

## Tips

Create a link to execute silently with 
'''powershell.exe -windowstyle hidden -noexit -ExecutionPolicy Bypass -File C:\my\file\path\save.ps1 -command backup -alert none'''

## Testing

### Requirments
 - Pester version 5.5.0

### Running tests

Use the following command to run the tests
`Invoke-Pester -Output Detailed`

Use the following command to get code coverage metrics (for the save and restore functions)
`Invoke-Pester .\save.Tests.ps1 -CodeCoverage .\save.ps1`
