Set objShell = CreateObject("WScript.Shell")
Set objFSO = CreateObject("Scripting.FileSystemObject")

' Define the paths
scriptPath = objFSO.BuildPath(objFSO.GetParentFolderName(WScript.ScriptFullName), "TLDTools Lancher.bat")
iconPath = objFSO.BuildPath(objFSO.GetParentFolderName(WScript.ScriptFullName), "assets\simple_tools_tiny_icon.ico")
shortcutPath = objFSO.BuildPath(objFSO.GetParentFolderName(WScript.ScriptFullName), "TLDTools.lnk")

' Create the shortcut
Set objShortcut = objShell.CreateShortcut(shortcutPath)
objShortcut.TargetPath = scriptPath
objShortcut.IconLocation = iconPath
objShortcut.Save
